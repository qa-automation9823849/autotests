package api.steps;

import api.config.base.requestImpl.CommonRequestHandler;
import api.config.configuration.BooksConfig;
import api.dto.response.books.BookResponseDTO;
import api.dto.response.books.BooksResponseDTO;
import api.dto.response.books.IsbnResponseDTO;
import io.qameta.allure.Step;

import static api.config.builders.BookBuilders.book;
import static api.config.builders.BookBuilders.userReplaceBook;
import static api.config.specification.ResponseSpec.created;
import static api.config.specification.ResponseSpec.noContent;
import static api.config.specification.ResponseSpec.ok;
import static org.aeonbits.owner.ConfigFactory.create;
import static org.aeonbits.owner.ConfigFactory.getProperties;

public class BookSteps {

    private final BooksConfig config = create(BooksConfig.class, getProperties());
    private final CommonRequestHandler request = new CommonRequestHandler();

    @Step("Получение всех книг в библиотеке")
    public BooksResponseDTO getAllBooks(){
        return request.get(config.getAllBooks()).spec(ok()).extract().as(BooksResponseDTO.class);
    }

    public BookResponseDTO getBook(String isbn) {
        return request.get(config.getBook(), isbn).spec(ok()).extract().as(BookResponseDTO.class);
    }

    @Step("Добавление книги пользователю")
    public void addBookIntoUser(String isbn){
        request.post(book(config.userId(), isbn), config.addBook()).spec(created()).extract().as(IsbnResponseDTO.class);
    }

    @Step("Удаление всех книг у пользователя")
    public void deleteBooks() {
        request.delete(config.deleteBooks(), config.userId()).spec(noContent());
    }

    @Step("Замена книги у пользователя")
    public void replaceBook(String newIsbn, String oldIsbn) {
        request.put(userReplaceBook(config.userId(), newIsbn), config.replaceBook() + oldIsbn);
    }
}
