package api.dto.response.account;

import api.dto.response.books.BookResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetUserResponseDTO {
    public String userId;
    public String username;
    public List<BookResponseDTO> books;
}
