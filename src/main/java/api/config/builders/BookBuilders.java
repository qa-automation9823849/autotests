package api.config.builders;

import api.dto.request.book.AddBookRequestDTO;
import api.dto.request.book.IsbnRequestDTO;
import api.dto.request.book.ReplaceBookRequestDTO;

import java.util.List;

public class BookBuilders {

    public static AddBookRequestDTO book(String id, String isbn) {
        return AddBookRequestDTO.builder()
                .userId(id)
                .collectionOfIsbns(List.of(
                        IsbnRequestDTO.builder()
                                .isbn(isbn)
                                .build())
                )
                .build();
    }

    public static ReplaceBookRequestDTO userReplaceBook (String id, String newIsbn) {
        return ReplaceBookRequestDTO.builder()
                .userId(id)
                .isbn(newIsbn)
                .build();
    }
}
