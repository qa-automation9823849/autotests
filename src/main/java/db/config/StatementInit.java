package db.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class StatementInit {

    private StatementInit() {

    }

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/habrdb";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "postgres";
    private static Connection connection = null;

    public static Connection getConnection() {
        checkDriver();
        if (connection == null) {
            synchronized (StatementInit.class) {
                if (connection == null) {
                    try {
                        connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
                    } catch (SQLException ex) {
                        System.out.println("Ошибка соединения");
                        ex.printStackTrace();
                    }
                }
            }
        }

        if (connection != null) {
            System.out.println("Соединение c базой данных успешное");
        } else {
            System.out.println("Ошибка соединения с базой данных");
        }
        return connection;
    }

    private static void checkDriver() {
        System.out.println("Проверка соединения с базой данных");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException exception) {
            System.out.println("PostgreSql jdbc driver не найден");
            exception.printStackTrace();
        }
        System.out.println("PostgreSQL JDBC driver успешно подключен");
    }

    public static void closeJDBCDriver() {
        try {
            connection.close();
            System.out.println("Соединения закрыто");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
