package db.dao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Car {

    private String automaker;
    private String transmission;
    private Double engine;
    private String fuel;
    private String color;
}
