package db.repository;

import db.dao.Car;

import java.util.List;

public interface UserRepository {
    List<Car> getCarByAutomaker (String automaker);
    Car getCarById(Long id);
    List<Car> getCarByAutomakerAndEngine(String automaker, Double engine);
    List<Car> getAllCars();
    void saveCar(Car car);
    void updateCar(Car carUpd, String automaker, String engine, String fuel);
    void deleteCar(String automaker, Double engine, String fuel);
    void deleteCarById(Long id);
}
