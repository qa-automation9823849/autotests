package db.repository;

import db.config.StatementInit;
import db.dao.Car;
import db.exceptions.UserNotFoundException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {
    @Override
    public List<Car> getCarByAutomaker(String automaker) {
        List<Car> cars = new ArrayList<>();

        try {
            String sql = "SELECT * FROM CARS WHERE automaker=?";
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, automaker);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                cars.add(new Car(rs.getString("automaker"), rs.getString("transmission"),
                        rs.getDouble("engine"), rs.getString("fuel"), rs.getString("color")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return cars;
    }

    @Override
    public Car getCarById(Long id) {
        String sql = "SELECT * FROM CARS WHERE id=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setLong(1, id);

            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new Car(rs.getString("automaker"), rs.getString("transmission"),
                        rs.getDouble("engine"), rs.getString("fuel"), rs.getString("color"));
            } else {
                throw new UserNotFoundException("Авто не найдено в базе данных");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Car> getCarByAutomakerAndEngine(String automaker, Double engine) {
        List<Car> cars = new ArrayList<>();

        try {
            String sql = "SELECT * FROM CARS WHERE automaker=? and engine=?";
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, automaker);
            preparedStatement.setDouble(2, engine);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                cars.add(new Car(rs.getString("automaker"), rs.getString("transmission"),
                        rs.getDouble("engine"), rs.getString("fuel"), rs.getString("color")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return cars;
    }

    @Override
    public List<Car> getAllCars() {
        List<Car> cars = new ArrayList<>();

        try {
            ResultSet rs = StatementInit.getConnection().createStatement().executeQuery("SELECT * FROM CARS");
            while (rs.next()) {
                cars.add(new Car(rs.getString("automaker"), rs.getString("transmission"),
                        rs.getDouble("engine"), rs.getString("fuel"), rs.getString("color")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return cars;
    }

    @Override
    public void saveCar(Car car) {
        String sql = "INSERT INTO CARS (automaker, transmission, engine, fuel, color) VALUES (?,?,?,?,?)";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, car.getAutomaker());
            preparedStatement.setString(2, car.getTransmission());
            preparedStatement.setDouble(3, car.getEngine());
            preparedStatement.setString(4, car.getFuel());
            preparedStatement.setString(5, car.getColor());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateCar(Car car, String automaker, String engine, String fuel) {
        String sql = "UPDATE CARS SET automaker=?, transmission=?, engine=?, fuel=?, color=?" +
                " WHERE automaker=? AND engine=? AND fuel=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, car.getAutomaker());
            preparedStatement.setString(2, car.getTransmission());
            preparedStatement.setDouble(3, car.getEngine());
            preparedStatement.setString(4, car.getFuel());
            preparedStatement.setString(5, car.getColor());
            preparedStatement.setString(6, automaker);
            preparedStatement.setDouble(7, Double.parseDouble(engine));
            preparedStatement.setString(8, fuel);
            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void deleteCar(String automaker, Double engine, String fuel) {
        String sql = "DELETE FROM CARS WHERE automaker=? AND engine=? AND fuel=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, automaker);
            preparedStatement.setDouble(2, engine);
            preparedStatement.setString(3, fuel);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void deleteCarById(Long id) {
        String sql = "DELETE FROM CARS WHERE id=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
