import db.dao.Car;
import db.repository.UserRepositoryImpl;
import org.testng.annotations.Test;

public class DatabaseTest extends BaseTest {
    @Test
    void homeWork17() {
//      Сохранение авто в БД
        Car carSave = new Car("volvo", "manual", 2.4, "GAS", "black");
        new UserRepositoryImpl().saveCar(carSave);

//      Обновление Авто в БД
        Car carUpd = new Car("audi", "manual", 2.4, "GAS", "black");
        new UserRepositoryImpl().updateCar(carUpd, "audi", String.valueOf(1.0), "GAS");

//      Удаление Авто из БД
        new UserRepositoryImpl().deleteCar("audi", 2.0,"DIESEL");
        new UserRepositoryImpl().deleteCarById(2L);

//      Запрос (поиск)  Авто в БД
        System.out.println(new UserRepositoryImpl().getCarByAutomaker("bmw"));
        System.out.println(new UserRepositoryImpl().getCarById(2L));
        System.out.println(new UserRepositoryImpl().getAllCars());
        System.out.println(new UserRepositoryImpl().getCarByAutomakerAndEngine("audi",3.0));
    }
}
