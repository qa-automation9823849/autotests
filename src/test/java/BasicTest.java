import api.dto.response.books.BookResponseDTO;
import api.exceptions.BookNotFoundException;
import io.qameta.allure.Epic;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import api.steps.BookSteps;
import api.steps.UserSteps;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

@Epic("Основной тест")
public class BasicTest {
//
//    private final String BOOK_TITLE = "Eloquent JavaScript, Second Edition";
//
//    @Test
//    void start() {
//        var tokenDto = new UserSteps().generateToken();
//
//        assertThat(tokenDto.getResult()).isEqualTo("User authorized successfully.");
//    }
    //// Запрос всех книг
//    @Test
//    void getBookIsbnTest() {
//        System.out.println();
//        System.out.printf("Ищем книгу по Title -\"%s\" и выводим её Isbn%n", BOOK_TITLE);
//        System.out.println();
//        var book = new BookSteps().getAllBooks().getBooks();
////
//// Ищем книгу по Title и выводим её Isbn
//        var bookIsbn = book.stream()
//                .filter(BookTitle -> BookTitle.getTitle().equals(BOOK_TITLE))
////                .filter(BookTitle -> BookTitle.getTitle().equals("Git Pocket Guide"))
//                .map(BookResponseDTO::getIsbn)
//                .findFirst()
//                .orElseThrow(() -> new BookNotFoundException("Not found this book"));
//
//        System.out.println();
//        System.out.printf("Выводим Isbn найденной книги - \"%s\"", bookIsbn);
//        System.out.println();
//        System.out.println();
//        System.out.printf("Проверяем книгу по найденному Isbn = \"%s\" ", bookIsbn);
//        System.out.println();
//
//// Проверяем книгу по найденному Isbn
//        var checkBook = new BookSteps().getBook(bookIsbn);
//
//        assertThat(BOOK_TITLE).isEqualTo(checkBook.getTitle());
//
//        System.out.printf("Выводим Title проверяемой книги по Isbn = \"%s\":",bookIsbn);
//        System.out.println();
//        System.out.printf("Title = \"%s\"", checkBook.getTitle());
//    }
//
//
    //    Добавляем книгу юзеру
//    @Test(retryAnalyzer = RetryAnalyzer.class)
//    void shouldBeAddedOneBookIntoUser() {
//        var isbn = new BookService().getIsbnOfBook(BOOK_TITLE);
//
//        new BookSteps().addBookIntoUser(isbn);
//
//        var user = new UserSteps().getUser();
//
//        var bookTitle = user.getBooks().stream()
//                .map(BookResponseDTO::getTitle)
//                .filter(title -> title.equals(BOOK_TITLE))
//                .findFirst()
//                .orElseThrow(() -> new BookNotFoundException(format("Book - %s is not found", BOOK_TITLE)));
//
//        assertThat(bookTitle).as("Title is not correct").isEqualTo(BOOK_TITLE);
//    }
//
//    @Test
//    void homeWork14() {
//
//        BookSteps bookSteps = new BookSteps();
//        UserSteps userSteps = new UserSteps();
//
//        //Удаляем все книги
//        bookSteps.deleteBooks();
//
//        //Получаем все книги
//        var book = bookSteps.getAllBooks().getBooks();
//
//        //Получаем все книги у юзера
//        var userIsbnList = userSteps.getUser().getBooks().stream()
//                .map(BookResponseDTO::getIsbn).toList();
//
//        //Выбираем любую книгу, для добавления юзеру, которой у него ещё нету
//        var bookIsbn = book.stream()
//                .map(BookResponseDTO::getIsbn)
//                .filter(isbn -> !userIsbnList.contains(isbn))
//                .findFirst()
//                .orElseThrow(() -> new BookNotFoundException("Not found this book"));
//
//        //Добавляем выбранную книгу юзеру
//        bookSteps.addBookIntoUser(bookIsbn);
//
//
//        //Проверяем добавилась ли книга пользователю
//        var bookIsbnCheck = userSteps.getUser().getBooks().stream()
//                .map(BookResponseDTO::getIsbn)
//                .filter(isbn -> isbn.equals(bookIsbn))
//                .findFirst()
//                .orElseThrow(() -> new BookNotFoundException(format("Book - %s is not found", bookIsbn)));
//
//        assertThat(bookIsbnCheck).as("Isbn is not found").isEqualTo(bookIsbn);
//
//
//        //Выбираем из списка книг новую книгу для замены
//        var newBookIsbn = book.stream()
//                .map(BookResponseDTO::getIsbn)
//                .filter(isbn -> !bookIsbnCheck.equals(isbn))
//                .findFirst()
//                .orElseThrow(() -> new BookNotFoundException("Not found this book"));
//
//        //Меняем книгу
//        bookSteps.replaceBook(newBookIsbn, bookIsbn);
//        assertThat(bookIsbnCheck).as("Isbn has not been changed").isEqualTo(bookIsbn);
//
//        //Проверяем, заменили ли книгу на новую
//        var newBookIsbnCheck = userSteps.getUser().getBooks().stream()
//                .map(BookResponseDTO::getIsbn)
//                .filter(isbn -> isbn.equals(newBookIsbn))
//                .findFirst()
//                .orElseThrow(() -> new BookNotFoundException(format("Book - %s is not found", newBookIsbn)));
//
//        assertThat(newBookIsbnCheck).as("Isbn is not found").isEqualTo(newBookIsbn);
//
//
//        //Удаляем все книги
//        bookSteps.deleteBooks();
//
//    }


    ////////// Домашняя работа 17
//    @Test(groups = {"Regression", "Smoke"}, description = "Получение пользователя по айди и проверка имени")
//    void shouldBeUserReturned() {
//        var user = new UserSteps().getUser();
//        assertThat(user.getUsername()).as("Username is not correct").isEqualTo("Zheka");
//    }


    @Story("[1232-xml] Как пользователь, я хочу добавить книгу, а затем заменить ее на новую.")
    @Test(dataProvider = "Books", groups = {"Api", "Smoke"}, description = "Добавление книги, затем замена её на новую")
    void homeWork16(String firstBook, String secondBook) {

        BookSteps bookSteps = new BookSteps();
        UserSteps userSteps = new UserSteps();

        //Добавляем выбранную книгу юзеру
        bookSteps.addBookIntoUser(firstBook);


        //Проверяем добавилась ли книга пользователю
        var bookIsbnCheck = userSteps.getUser().getBooks().stream()
                .map(BookResponseDTO::getIsbn)
                .filter(isbn -> isbn.equals(firstBook))
                .findFirst()
                .orElseThrow(() -> new BookNotFoundException(format("Book - %s is not found", firstBook)));

        assertThat(bookIsbnCheck).as("Isbn is not found").isEqualTo(firstBook);

        //Меняем книгу
        bookSteps.replaceBook(secondBook, firstBook);
        assertThat(bookIsbnCheck).as("Isbn has not been changed").isEqualTo(firstBook);

        //Проверяем, заменили ли книгу на новую
        var newBookIsbnCheck = userSteps.getUser().getBooks().stream()
                .map(BookResponseDTO::getIsbn)
                .filter(isbn -> isbn.equals(secondBook))
                .findFirst()
                .orElseThrow(() -> new BookNotFoundException(format("Book - %s is not found", secondBook)));

        assertThat(newBookIsbnCheck).as("Isbn is not found").isEqualTo(secondBook);

    }

    @DataProvider(name = "Books")
    public Object[][] getBooks() {
        return new Object[][]{
                {"9781449325862", "9781449331818"}
//                {"9781449337711", "9781449365035"}
        };
    }

    @BeforeMethod(groups = {"Regression", "Smoke", "Api"}, description = "Удаление всех книг у пользователя перед" +
            " началом тестов")
    void clearAllBooks() {
        //Удаляем все книги
        new BookSteps().deleteBooks();
    }
}
